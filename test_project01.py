def add_numbers(a, b):
    return a + b

def test_add_numbers():
    assert add_numbers(5, 10) == 15
    assert add_numbers(-5, 10) == 5
    assert add_numbers(0, 0) == 0